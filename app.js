var http = require('http');
var express = require('express'),
    app = module.exports.app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();
var upload = require('express-fileupload');

var mongoose = require('mongoose');
mongoose.connect('mongodb://kuveytturk:kuveytturk1234@ds135796.mlab.com:35796/kuveytturk', { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended:true }))
app.use(upload());

app.use("/auth", require("./page/auth.js"))
app.use("/profile", require("./page/profile.js"))
app.use("/api", require("./page/api.js"))

app.use(express.static('uploads'))

app.get("/", function(req, res){
    res.send('Kuveyttürk Api sunucusu');
});

var server = http.createServer(app);
server.listen(80);