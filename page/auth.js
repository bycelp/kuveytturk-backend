var express = require('express');
var router = express.Router();
var md5 = require('md5');
var User = require('../models/Users');
var jwt = require('jsonwebtoken');
var SECRET = "123456";

router.post("/register" , function (req,res) {
    var newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        telephone: req.body.telephone,
        oneSignal: req.body.oneSignal,
        password: md5(req.body.password)
    });

    newUser.save(function (err,reg_res) {
        if(err){
            return res.status(400).send({ 'result': 400,'status': 'FAIL','message': err });
        }else{
            var token = jwt.sign({user_id: reg_res._id, oneSignal: req.body.oneSignal}, SECRET);
            return res.status(200).send({ 'result': 200, 'status': 'SUCCESS', 'message': 'İşleminiz başarıyla gerçekleşmiştir.', 'access_token': token, 'response': reg_res });
        }
    });
});


router.post("/login", function(req,res){
    var telephone = req.body.telephone;
    var password = md5(req.body.password);

    User.findOne({ telephone: telephone, password: password }, function (err,find_res) {
        if(err){
           return res.status(400).send({ 'result': 400, 'status': 'FAIL', 'message': error(err) });
        }
        if(find_res){
            var token = jwt.sign({user_id: find_res._id, oneSignal: find_res.oneSignal }, SECRET);
            return res.status(200).send({ 'result': 200, 'status': 'SUCCESS', 'message': 'İşleminiz başarıyla gerçekleşmiştir.', 'access_token': token, 'response': find_res });
        }else{
            return res.status(400).send({ 'result': 400, 'status': 'FAIL', 'message': 'Telefon adresi veya şifre hatalı!' });
        }
    });
});


router.get('/verification', function(req,res){
    var token = req.headers.authorization;
    jwt.verify(token,SECRET, function(err, decoded) {
        if (err) {
            return res.status(401).send({ 'result': 401, 'status': 'LOGIN_FAIL', 'message': 'Lütfen tekrar giriş yap!'});
        } else {
            return res.status(200).send({ 'result': 200, 'status': 'SUCCESS', 'message': 'Başarıyla giriş yapıldı', 'access_token': token});
        }
    });
});

function error(err){
    if (err.name == 'ValidationError') {
        for (field in err.errors) {
            return err.errors[field].message;
        }
    }else if(err.name == 'MongoError'){
        if(err.code == 11000){
            return 'Bu Telefon zaten kullanılıyor!'
        }else{
            return 'Bir Hata oluştu. Lütfen daha sonra tekrar dene!'
        }
    }else{
        return 'Bir Hata oluştu. Lütfen daha sonra tekrar dene!'
    }
}




module.exports = router;