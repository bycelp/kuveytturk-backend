var express = require('express');
var router = express.Router();
var md5 = require('md5');
var jwt = require('jsonwebtoken');
var User = require('../models/Users');
var tokenToUserId = require('../models/Token');
var axios = require("axios");
var url = 'https://apitest.kuveytturk.com.tr/prep/v1/';
var code = '7cb40fdd4856fb843ce20e6deed59a7d41a74e8e95cfee8a8715c3978a0ae29c';

router.get('/account/:id', function(req,res){
  axios.get(url+'accounts/'+req.params.id, { headers: { Authorization: 'Bearer '+code } })
  .then(function (response) {
    res.send(response.data);
  });
});

router.get('/virtualcardInfo', function(req,res){
  axios.post(url+'cards/virtualcardinfo', { cardNumber: 5188969443663849 }, { headers: { Authorization: 'Bearer '+code  } })
  .then(function (response) {
    return res.send({'code': 200, 'status': 'SUCCESS', 'message': 'İşlem başarılı!', 'response': response});
  })
  .catch(function (error) {
    return res.send({'code': 400, 'status': 'FAIL', 'message': 'işlem başarısız!', 'err': error});
  });
});

router.post('/limitUpdate', function(req,res){
  var cardNumber = req.body.cardNumber;
  var limit = req.body.limit;

  axios.post(url+'cards/virtualcardlimitupdate', {cardNumber, limit}, { headers: { Authorization: 'Bearer '+code  } })
  .then(function (response) {
    return res.send({'code': 200, 'status': 'SUCCESS', 'message': 'Limitler başarıyla güncellendi!'});
  })
  .catch(function (error) {
    return res.send({'code': 400, 'status': 'FAIL', 'message': 'Limit güncellenirken hata oluştu!'});
  });

});


router.get('/transactions', function(req,res){
  axios.get(url+'/accounts/1/transactions', { headers: { Authorization: 'Bearer '+code  } })
  .then(function (response) {
    return res.send({'code': 200, 'status': 'SUCCESS', 'message': 'İşlem başarılı!', 'response': response});
  })
  .catch(function (error) {
    return res.send({'code': 400, 'status': 'FAIL', 'message': 'işlem başarısız!'});
  });
});


router.post('/cardDetail', function(req,res){
  axios.post(url+'/cards/carddetail', req.body, { headers: { Authorization: 'Bearer '+code  } })
  .then(function (response) {
    return res.send({'code': 200, 'status': 'SUCCESS', 'message': 'İşlem başarılı!', 'response': response});
  })
  .catch(function (error) {
    return res.send({'code': 400, 'status': 'FAIL', 'message': 'işlem başarısız!'});
  });
});

module.exports = router;