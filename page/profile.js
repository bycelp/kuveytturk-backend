var express = require('express');
var router = express.Router();
var md5 = require('md5');
var jwt = require('jsonwebtoken');
const axios = require('axios');
var User = require('../models/Users');
var tokenToUserId = require('../models/Token');

router.get('/me', tokenToUserId, function(req,res){
    const { user_id } = req;
    User.findOne({ _id: user_id }, ' -password -__v', function (err,user_res) {
        if(err){
           return res.status(400).send({ 'result': 400, 'status': 'FAIL', 'message': error(err) });
        }
        if(user_res){
            return res.status(200).send({ 'result': 200, 'status': 'SUCCESS', 'message': 'İşleminiz başarıyla gerçekleşmiştir.', 'response': user_res });
        }else{
            return res.status(400).send({ 'result': 400, 'status': 'FAIL', 'message': 'Kullanıcı bulunamadı!' });
        }
    });
});




module.exports = router;