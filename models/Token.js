var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var SECRET = "123456";
var User = require('./Users');

function tokenToUserId(req, res, next){
    var token = req.headers.authorization;
    jwt.verify(token,SECRET, function(err, decoded) {
        if (err) {
            return res.status(401).send({ 'result': 401, 'status': 'LOGIN_FAIL', 'message': 'Lütfen tekrar giriş yap!'});
        } else {
            var user_id = decoded.user_id;
            User.findOne({ _id: user_id }, '-_id -password -__v', function (err,user_res) {
                if(err){
                   return res.status(400).send({ 'result': 400, 'status': 'FAIL', 'message': error(err) });
                }
                if(user_res){
                    req.user_id = user_id
                    req.user = user_res
                    next()
                }else{
                    return res.status(401).send({ 'result': 401, 'status': 'LOGIN_FAIL', 'message': 'Lütfen tekrar giriş yap!'});
                }
            })
        }
    });
}

module.exports = tokenToUserId;