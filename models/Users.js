var mongoose = require('mongoose');
var validate = require('mongoose-validator');

const Schema = mongoose.Schema;
const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        trim: true,
        required: true,
    },
    telephone: {
        type: Number,
        required: true,
        trim: true,
    },
    profilePhoto: {
        type: String,
        default: 'https://i.hizliresim.com/lqBXgX.png'
    },
    upBalance: {
        type: Number,
        default: 0
    },
    downBalance: {
        type: Number,
        default: 0
    },
    balance: {
        type: Number,
        default: 0
    },
    payment: {
        type: Number,
        default: 0
    },
    lastAction: [],
    oneSignal: {
        type: Number,
        default: 0
    },
    dateCreated: {
        type: Date,
        default: new Date(),
    },
});
const User = mongoose.model('User', userSchema);

module.exports = User;